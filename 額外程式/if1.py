#coding: utf8
import cherrypy, sys
def file_get_contents(filename):
    with open(filename, encoding="utf-8") as f:
        return f.read()

class HelloWorld(object):
    _cp_config = {
    # 配合 utf-8 格式之表單內容
    # 若沒有 utf-8 encoding 設定,則表單不可輸入中文
    'tools.encode.encoding': 'utf-8'
    }
 
    def index(self):
        return "<form method=\"post\" action=\"doAct\"> \
        分組程式，會依照每組的最小座號來決定組別順序<br />注意:必須把最小座號打在組員一的位置<br />組:<br />組員一:<input type=\"text\" name=\"inp\"><br /> \
        組員二:<input type=\"text\" name=\"inp2\"><br /> \
        組:<br />組員一:<input type=\"text\" name=\"inp3\"><br /> \
        組員二:<input type=\"text\" name=\"inp4\"><br /> \
        組:<br />組員一:<input type=\"text\" name=\"inp5\"><br /> \
        組員二:<input type=\"text\" name=\"inp6\"><br /> \
        <input type=\"submit\" value=\"送出\"></form>"
    index.exposed = True
    def doAct(self, inp=None, inp2=None, inp3=None, inp4=None, inp5=None, inp6=None):
        #inp 變數即為表單值, 其格式為字串
        if inp < inp2 :
            if inp3 < inp4 :
                if inp5 < inp6 :
                    if inp < inp3 :
                        if inp3 < inp5 :
                            file =open("file1.txt", "a", encoding="utf-8")
                            file.write("你的大好組員是:"+str(inp)+","+str(inp2)+","+str(inp3)+","+str(inp4)+","+str(inp5)+","+str(inp6)+"\n")
                            file.close()
                            資料 = file_get_contents("file1.txt")
                            資料 = 資料.replace("\n", "<br />")
                            return "第一組:"+str(inp)+","+str(inp2)+"<br />"+"第二組:"+str(inp3)+","+str(inp4)+"<br />"+ "第三組:"+str(inp5)+","+str(inp6)+"<br />"
                        else:
                            if inp < inp5 :
                                file =open("file1.txt", "a", encoding="utf-8")
                                file.write("你的大好組員是:"+str(inp)+","+str(inp2)+","+str(inp3)+","+str(inp4)+","+str(inp5)+","+str(inp6)+"\n")
                                file.close()
                                資料 = file_get_contents("file1.txt")
                                資料 = 資料.replace("\n", "<br />")
                                return "第一組:"+str(inp)+","+str(inp2)+"<br />"+"第二組:"+str(inp5)+","+str(inp6)+"<br />"+ "第三組:"+str(inp3)+","+str(inp4)+"<br />"  
                            else:
                                file =open("file1.txt", "a", encoding="utf-8")
                                file.write("你的大好組員是:"+str(inp)+","+str(inp2)+","+str(inp3)+","+str(inp4)+","+str(inp5)+","+str(inp6)+"\n")
                                file.close()
                                資料 = file_get_contents("file1.txt")
                                資料 = 資料.replace("\n", "<br />")
                                return "第一組:"+str(inp5)+","+str(inp6)+"<br />"+"第二組:"+str(inp)+","+str(inp2)+"<br />"+ "第三組:"+str(inp3)+","+str(inp4)+"<br />"  
                    else:
                        if inp < inp5 :
                            file =open("file1.txt", "a", encoding="utf-8")
                            file.write("你的大好組員是:"+str(inp)+","+str(inp2)+","+str(inp3)+","+str(inp4)+","+str(inp5)+","+str(inp6)+"\n")
                            file.close()
                            資料 = file_get_contents("file1.txt")
                            資料 = 資料.replace("\n", "<br />")
                            return "第一組:"+str(inp3)+","+str(inp4)+"<br />"+"第二組:"+str(inp)+","+str(inp2)+"<br />"+ "第三組:"+str(inp5)+","+str(inp6)+"<br />"
                        else:
                            if inp3 < inp5 :
                                file =open("file1.txt", "a", encoding="utf-8")
                                file.write("你的大好組員是:"+str(inp)+","+str(inp2)+","+str(inp3)+","+str(inp4)+","+str(inp5)+","+str(inp6)+"\n")
                                file.close()
                                資料 = file_get_contents("file1.txt")
                                資料 = 資料.replace("\n", "<br />")
                                return "第一組:"+str(inp3)+","+str(inp4)+"<br />"+"第二組:"+str(inp5)+","+str(inp6)+"<br />"+ "第三組:"+str(inp)+","+str(inp2)+"<br />"
                            else:
                                file =open("file1.txt", "a", encoding="utf-8")
                                file.write("你的大好組員是:"+str(inp)+","+str(inp2)+","+str(inp3)+","+str(inp4)+","+str(inp5)+","+str(inp6)+"\n")
                                file.close()
                                資料 = file_get_contents("file1.txt")
                                資料 = 資料.replace("\n", "<br />")
                                return "第一組:"+str(inp5)+","+str(inp6)+"<br />"+"第二組:"+str(inp3)+","+str(inp4)+"<br />"+ "第三組:"+str(inp)+","+str(inp2)+"<br />"  
                else:
                    return "未依正確格式輸入"
            else:
                return "未依正確格式輸入"
        else:
            return "未依正確格式輸入"

        file =open("file1.txt", "a", encoding="utf-8")
        file.write("你的大好組員是:"+str(inp)+","+str(inp2)+","+str(inp3)+","+str(inp4)+","+str(inp5)+","+str(inp6)+"\n")
        file.close()
        資料 = file_get_contents("file1.txt")
        資料 = 資料.replace("\n", "<br />")
        
        return 321
    doAct.exposed = True

    def default(self):
        sys.exit()
    default.exposed = True
 
cherrypy.server.socket_port = 8083
cherrypy.server.socket_host = '120.113.123.20'
cherrypy.quickstart(HelloWorld())